export class CreateUserDto {
  id: number;
  email: string;
  password: string;
  fullName: string;
  gender: string;
  roles: string;
  image: string;
}
